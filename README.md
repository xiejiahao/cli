# xfe-cli

![license](https://img.shields.io/badge/%E8%AE%B8%E5%8F%AF%E5%8D%8F%E8%AE%AE-MIT-blue) <img src="https://badgen.net/npm/v/xfe-cli" alt="Npm Version" maxretrytimes="3" class="m-1 transition-all duration-1000"> <img src="https://badgen.net/npm/node/xfe-cli" alt="Node Version" maxretrytimes="3" class="m-1 transition-all duration-1000"> <img src="https://badgen.net/npm/dw/xfe-cli" alt="Npm Weekly Downloads" maxretrytimes="3" class="m-1 transition-all duration-1000">

**English** | [中文](https://gitee.com/xiejiahao/cli/blob/master/README.zh-CN.md)

![Preview](https://gitee.com/xiejiahao/cli/raw/master/preview.jpg)

#### Introduction

- A command-line tool for managing front-end development templates, designed for easy one-click extraction, installation, and operation.

- Developed in 2022 without packing or building processing, it can be modified and expanded based on the source code.

#### Install

```
npm i xfe-cli -g
```

#### Directions for use

- When you want to open a new project, in the specified folder, open the command line and type `xfe-cli` to do so.。

- The language command `xfe-cli -l en` can be set. The default value is `cn`. Only Chinese and English are supported。

#### Matters needing attention

- You should start with the **project configuration**, following the default json content and format, configure your own related projects。

- The configuration can be changed at any time, it will be saved in the global **cli** directory, you can update the project template at any time。
