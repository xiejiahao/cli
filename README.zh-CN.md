# xfe-cli

![许可协议](https://img.shields.io/badge/%E8%AE%B8%E5%8F%AF%E5%8D%8F%E8%AE%AE-MIT-blue) <img src="https://badgen.net/npm/v/xfe-cli" alt="Npm Version" maxretrytimes="3" class="m-1 transition-all duration-1000"> <img src="https://badgen.net/npm/node/xfe-cli" alt="Node Version" maxretrytimes="3" class="m-1 transition-all duration-1000"> <img src="https://badgen.net/npm/dw/xfe-cli" alt="Npm Weekly Downloads" maxretrytimes="3" class="m-1 transition-all duration-1000">

**中文** | [English](https://gitee.com/xiejiahao/cli/blob/master/README.md)

![Preview](https://gitee.com/xiejiahao/cli/raw/master/preview.png)

#### 介绍

- 用于管理前端开发模板的命令行工具，方便一键拉取、安装和运行。

- 开发于2022年，未做打包构建处理，可以基于源码自行改造拓展。

#### 安装教程

```
npm i xfe-cli -g
```

#### 使用说明

- 当你打算新开项目，在指定文件夹中，打开命令行，输入`xfe-cli`，即可进行操作。

- 支持设置语言命令`xfe-cli -l en`，默认为`cn`，只支持中文和英文。

#### 注意事项

- 应该先进行**项目配置**，按照默认的 json 内容和格式，配置好自己相关的项目。

- 配置可以随时进行更改，它会保存在全局 cli 目录，你可以随时更新项目模板。
